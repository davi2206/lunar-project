# Lunar Project

Proof of Concept project, as part of recruitment process. 

## Description

Task:
Create a Django application, with simple web front-end and postgres backend, to allow basic login and presentation of Lunar phases. 
Must be containerized with Docker

## Installation
### Requirements

- Docker
- Compose

### Setup

This project is meant to set itself up, and run, with no configuration. 
If the requirements are met, simply pull the latest version of the repo, and run the run.sh script from the terminal. 

## Support

If you need support for this project, the developer have failed their task

## Roadmap

- Python Server API. Serve data on endpoint
- Python Server DB connection. Get data from DB
- Python Server Lunar Phases? Get the phases, based on the date, and serve on API endpoint
- Create deployable Python Server container
- Postgres DB, table and data
- Postgress setup script: Create DB, tables and 2 users
- Create deployable Postgres DB Container
- Python/Django basic web-frontend. First view: Loginscreen, that does nothing
- Python/Django save creadentials in cache (?)
- Python/Django Lunar Phases view: GET data from server, with Basec HTTP Auth
- Create deployable Python/Django container


## Contributing

This is a one-man project. If you desire to help out, we sincerely thank you, but respectfully decline

## Authors and acknowledgment
Author: Davi2206
Product Owner: Vasili

## License
Free-for-all
