#!/bin/bash
# this script is run when the docker container is built

echo "We did a database thing"

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE DATABASE lunar;
	\c lunar
	CREATE TABLE IF NOT EXISTS users (user_id serial PRIMARY KEY,email varchar(255) UNIQUE NOT NULL,hash_cred varchar(255) UNIQUE NOT NULL);
	INSERt INTO users (email, hash_cred) VALUES ('dmkolind@hotmail.com','ZG1rb2xpbmRAaG90bWFpbC5jb206c2VjcmV0cw==');
	INSERT INTO users (email, hash_cred) VALUES ('vasilii@safeex.com','dmFzaWxpaUBzYWZlZXguY29tOm1hZ2ljX3dvcmRz');
EOSQL
