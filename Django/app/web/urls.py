"""
https://docs.djangoproject.com/en/3.2/topics/http/urls/
"""

from . import views
from django.urls import path

urlpatterns = [
    path('', views.home),
    path('luna', views.lunar),
    # API endpoints
    path('api/lunarphase', views.getLunarPhases),
]
