import os
import psycopg2


def getLunarPhase(now):
    daysInYear = 365.25 * now.year
    daysInMonth = 30.6 * now.month
    totalDays = daysInYear + daysInMonth + now.day - 694039.09 # Don't ask why. The internet said so...

    lunarCyclesPassed = totalDays / 29.5305882 # Dividing the total days by the days in one lunar cycle
    currentLunarState = lunarCyclesPassed - int(lunarCyclesPassed)
    lunarPhase = round(currentLunarState * 8)

    if lunarPhase == 8: lunarPhase = 0

    lunarPhases = {
        0:'New',
        1:'Waxing Crescent',
        2:'First Quarter',
        3:'Waxing Gibbous',
        4:'Full',
        5:'Waining Gibbous',
        6:'Last Quarter',
        7:'Waning Crescent',
    }

    return lunarPhases[lunarPhase]


def validateDbUser(user_cred):
    conn = None

    try:
        conn = psycopg2.connect(
            host=os.environ.get('POSTGRES_HOST'),
            user=os.environ.get('POSTGRES_USER'),
            password=os.environ.get('POSTGRES_PASSWORD'),
            database="lunar",
            port=os.environ.get('POSTGRES_PORT'),
        )

        query = "SELECT * FROM Users WHERE hash_cred='{}'".format(user_cred)

        cur = conn.cursor()
        cur.execute(query)

        data = cur.fetchone()
        conn.close()
        return data
    except:
        return None
