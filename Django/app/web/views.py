import base64

from django.http import HttpResponse
from django.shortcuts import redirect
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.shortcuts import render
from datetime import datetime
from django.templatetags.static import static

from .support_files import support_functions as support

def home(request):
    if request.method == 'POST':

        email = request.POST.get('email', '_')
        password = request.POST.get('pass', '_')
        user = base64.b64encode(str.encode('{}:{}'.format(email,password))).decode()

        request.session['session_user'] = user

        return redirect('./luna')
    return render(request, 'index.html')
    
def lunar(request):
    cred = request.session.get('session_user')

    valid_user = support.validateDbUser(cred)

    if valid_user is not None:
        lunarPhase = support.getLunarPhase(datetime.now())

        path = 'moons/{}.png'.format(lunarPhase)
        img_url = static(path)

        context = {
            'phase':lunarPhase,
            'img_url':img_url,
        }
        return render(request, 'lunar.html', context)

    return redirect('.', 403)


# API endpoints

@api_view(['GET'])
def getLunarPhases(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION', '')
    token_type, _, credentials = auth_header.partition(' ')

    valid_user = support.validateDbUser(credentials)
    if token_type != 'Basic' or valid_user is None:
        return HttpResponse(status=401)

    lunarPhase = support.getLunarPhase(datetime.now())

    moon = {
        'Moon':'Luna',
        'Phase':lunarPhase
    }
    return Response(moon)
